#include <benchmark/benchmark.h>
#include "workers.hpp"

void unique(benchmark::State &state)
{
	a test;
	for(auto _ : state)
	{
		test.start();
		test.finish();
	}
}

void shared(benchmark::State &state)
{
	b test;
	for(auto _ : state)
	{
		test.start();
		test.finish();
	}
}

void shared_aligned_bool(benchmark::State &state)
{
	c test;
	for(auto _ : state)
	{
		test.start();
		test.finish();
	}
}

void unique_any(benchmark::State &state)
{
	a_alt test;
	for(auto _ : state)
	{
		test.start();
		test.finish();
	}
}

BENCHMARK(unique);
BENCHMARK(shared);
BENCHMARK(unique_any);
BENCHMARK(shared_aligned_bool);

BENCHMARK_MAIN();
