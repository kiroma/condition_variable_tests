cmake_minimum_required(VERSION 3.8.2)

project(condVarBench LANGUAGES CXX)

add_executable(bench main.cpp)

find_package(Threads REQUIRED)
find_package(benchmark REQUIRED)

target_link_libraries(bench benchmark::benchmark benchmark::benchmark_main Threads::Threads)

set_property(TARGET bench PROPERTY CXX_STANDARD 17)
set_property(TARGET bench PROPERTY CXX_STANDARD_REQUIRED ON)
