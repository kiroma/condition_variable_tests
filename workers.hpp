#pragma once
#include <thread>
#include <atomic>
#include <vector>
#include <string>
#include <mutex>
#include <shared_mutex>
#include <condition_variable>

class base
{
	const size_t hc = (std::thread::hardware_concurrency() < 2 ? 1 : std::thread::hardware_concurrency() - 1);
	std::vector<std::thread> tp;
	std::basic_string<bool> ready;
	std::condition_variable recv, send;
	std::mutex r_mtx, s_mtx;
	bool terminate{false};
	unsigned int finished;
	static void worker(bool &ready, const bool &terminate, std::condition_variable &recv, std::condition_variable &send, std::mutex &r_mtx, std::mutex &s_mtx, unsigned int &finished, const unsigned int target)
	{
		while(!terminate)
		{
			std::unique_lock<std::mutex> lk(r_mtx);
			recv.wait(lk, [&](){
				return ready || terminate;
			});
			if(terminate)
			{
				return;
			}
			ready = false;
			lk.unlock();
			std::lock_guard lg(s_mtx);
			if(++finished == target)
			{
				send.notify_all();
			}
		}
	}
public:
	void prepare()
	{
		finished = 0;
		std::lock_guard lg(r_mtx);
		for(auto &a : ready)
		{
			a = true;
		}
	}
	base()
	{
		ready.resize(hc);
		for(size_t i = 0; i < hc; ++i)
		{
			tp.emplace_back(worker, std::ref(ready[i]), std::cref(terminate), std::ref(recv), std::ref(send), std::ref(r_mtx), std::ref(s_mtx), std::ref(finished), hc);
		}
	}
	void finish()
	{
		std::unique_lock<std::mutex> lk(s_mtx);
		send.wait(lk, [&](){
			return finished == hc;
		});
	}
	void start()
	{
		prepare();
		recv.notify_all();
	}
	~base()
	{
		r_mtx.lock();
		terminate = true;
		r_mtx.unlock();
		recv.notify_all();
		for(auto &a : tp)
		{
			a.join();
		}
	}
};

class a : public base
{
};

class b : public base
{
	std::condition_variable_any recv;
	std::shared_mutex r_mtx;
	static void worker(bool &ready, const bool &terminate, std::condition_variable_any &recv, std::condition_variable &send, std::shared_mutex &r_mtx, std::mutex &s_mtx, unsigned int &finished, const unsigned int target)
	{
		while(!terminate)
		{
			std::shared_lock<std::shared_mutex> lk(r_mtx);
			recv.wait(lk, [&](){
				return ready || terminate;
			});
			if(terminate)
			{
				return;
			}
			ready = false;
			lk.unlock();
			std::lock_guard lg(s_mtx);
			if(++finished == target)
			{
				send.notify_all();
			}
		}
	}
};

class c : public b
{
	struct cs_bool
	{
	private:
		union{
			bool i;
			char a[64];
		};
	public:
		operator bool()
		{
			return i;
		}
		bool operator=(const bool rhs)
		{
			return i = rhs;
		}
	};
	std::vector<cs_bool> ready;
	static void worker(cs_bool &ready, const bool &terminate, std::condition_variable_any &recv, std::condition_variable &send, std::shared_mutex &r_mtx, std::mutex &s_mtx, unsigned int &finished, const unsigned int target)
	{
		while(!terminate)
		{
			std::shared_lock<std::shared_mutex> lk(r_mtx);
			recv.wait(lk, [&](){
				return ready || terminate;
			});
			if(terminate)
			{
				return;
			}
			ready = false;
			lk.unlock();
			std::lock_guard lg(s_mtx);
			if(++finished == target)
			{
				send.notify_all();
			}
		}
	}
};

class a_alt : public a
{
	std::condition_variable_any recv;
	static void worker(bool &ready, const bool &terminate, std::condition_variable_any &recv, std::condition_variable &send, std::mutex &r_mtx, std::mutex &s_mtx, unsigned int &finished, const unsigned int target)
	{
		while(!terminate)
		{
			std::unique_lock<std::mutex> lk(r_mtx);
			recv.wait(lk, [&](){
				return ready || terminate;
			});
			if(terminate)
			{
				return;
			}
			ready = false;
			lk.unlock();
			std::lock_guard lg(s_mtx);
			if(++finished == target)
			{
				send.notify_all();
			}
		}
	}
};
